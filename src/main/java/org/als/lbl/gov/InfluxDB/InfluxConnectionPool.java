package org.als.lbl.gov.InfluxDB;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.influxdb.InfluxDB;

/**
 * A pool of InfluxDB Objects.
 */
public class InfluxConnectionPool extends GenericObjectPool<InfluxDB> {

    public InfluxConnectionPool(PooledObjectFactory<InfluxDB> factory) {
        super(factory);
    }

    public InfluxConnectionPool(PooledObjectFactory<InfluxDB> factory,
                                GenericObjectPoolConfig config) {
        super(factory, config);
    }
}
