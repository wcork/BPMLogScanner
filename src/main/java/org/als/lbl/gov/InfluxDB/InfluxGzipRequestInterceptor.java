package org.als.lbl.gov.InfluxDB;


import com.google.common.util.concurrent.RateLimiter;
import com.squareup.okhttp.*;
import okio.BufferedSink;
import okio.GzipSink;
import okio.Okio;

import java.io.IOException;

/**
 * Okio interceptor for Influx. This will convert all requests to gzipRequest. Significantly reduces network traffic.
 */
public class InfluxGzipRequestInterceptor implements Interceptor {

    private RateLimiter rateLimiter = null;

    public InfluxGzipRequestInterceptor(RateLimiter rateLimiter) {
        this.rateLimiter = rateLimiter;
    }

    public InfluxGzipRequestInterceptor() {
        this.rateLimiter = null;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request originalRequest = chain.request();
        if (originalRequest.body() == null || originalRequest.header("Content-Encoding") != null) {
            return chain.proceed(originalRequest);
        }

        Request compressedRequest = originalRequest.newBuilder()
                .header("Content-Encoding", "gzip")
                .method(originalRequest.method(), gzipRequest(originalRequest.body()))
                .build();
        return chain.proceed(compressedRequest);
    }

    private RequestBody gzipRequest(final RequestBody body) {
        return new RequestBody() {
            @Override
            public MediaType contentType() {
                return body.contentType();
            }

            @Override
            public long contentLength() {
                return -1; // We don't know the compressed length in advance!
            }

            @Override
            public void writeTo(BufferedSink sink) throws IOException {
                BufferedSink gzipSink = Okio.buffer(new GzipSink(sink));
                body.writeTo(gzipSink);
                // Request to write from limiter.
                int permitLength = (int) gzipSink.buffer().size();
                if (rateLimiter != null && permitLength > 0) {
                    rateLimiter.acquire(permitLength);
                }
                gzipSink.close();
            }
        };
    }
}
