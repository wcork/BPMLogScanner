package org.als.lbl.gov.InfluxDB;

import com.google.common.util.concurrent.RateLimiter;
import com.squareup.okhttp.OkHttpClient;
import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import retrofit.client.OkClient;

import java.util.concurrent.TimeUnit;

/**
 * Factory wrapper for InfluxDB for use in Apache Commons Pool2
 */
public class InfluxFactory extends BasePooledObjectFactory<InfluxDB> {
    private final String influxURL;
    private final String influxUser;
    private final String influxPass;

    private final boolean compress;
    private RateLimiter rateLimiter = null;

    public InfluxFactory(String influxURL, String influxUser, String influxPass) {
        this(influxURL, influxUser, influxPass, false);
    }

    public InfluxFactory(String influxURL, String influxUser, String influxPass, RateLimiter rateLimiter) {
        this(influxURL, influxUser, influxPass, rateLimiter, false);
    }

    public InfluxFactory(String influxURL, String influxUser, String influxPass, boolean compress) {
        this.influxURL = influxURL;
        this.influxUser = influxUser;
        this.influxPass = influxPass;
        this.compress = compress;
    }

    public InfluxFactory(String influxURL, String influxUser, String influxPass,
                         RateLimiter rateLimiter, boolean compress) {
        this.influxURL = influxURL;
        this.influxUser = influxUser;
        this.influxPass = influxPass;
        this.rateLimiter = rateLimiter;
        this.compress = compress;
    }


    @Override
    public InfluxDB create() throws Exception {
        // TODO: Wait for InfluxDB Java 2.4 to give new Retrofit Builder.
        OkHttpClient clnt = new OkHttpClient();
        clnt.setWriteTimeout(5, TimeUnit.MINUTES);
        clnt.setReadTimeout(5, TimeUnit.MINUTES);
        if (this.compress) {
        // Configure influx to compress all data
            clnt.interceptors().add(new InfluxGzipRequestInterceptor(this.rateLimiter));
        }
        OkClient client = new OkClient(clnt);
        return InfluxDBFactory.connect(this.influxURL, this.influxUser, this.influxPass, client);
    }

    @Override
    public PooledObject<InfluxDB> wrap(InfluxDB obj) {
        return new DefaultPooledObject<>(obj);
    }

    @Override
    public void passivateObject(PooledObject<InfluxDB> pooledObject) {
        if(pooledObject.getObject().isBatchEnabled()) {
            pooledObject.getObject().disableBatch();
        }
    }
}
