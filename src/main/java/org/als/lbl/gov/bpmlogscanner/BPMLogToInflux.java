package org.als.lbl.gov.bpmlogscanner;

import com.google.common.util.concurrent.RateLimiter;
import org.als.lbl.gov.InfluxDB.InfluxConnectionPool;
import org.als.lbl.gov.InfluxDB.InfluxFactory;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.commons.validator.routines.UrlValidator;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Pong;
import retrofit.RetrofitError;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by wcork on 27/09/16.
 * Notes: TODO: Make comments and stuff.
 */
public class BPMLogToInflux {
    private static final Logger logger = Logger.getLogger("");
    private static final Path appPath = Paths.get("/opt/als/BPMLogScanner/");

    private static int NUM_THREADS = 1;
    private static int SAMPLE_SPACING_MILLISECONDS = 0;
    private static Path LOG_DIR;

    private static final UrlValidator urlValidator = new UrlValidator(new String[]{"http","https"},
                                                UrlValidator.ALLOW_2_SLASHES + UrlValidator.ALLOW_LOCAL_URLS);
    private static String influxURL;
    private static String influxUser;
    private static String influxPass;
    private static String influxDatabase;
    private static String influxMeasurement;
    private static String influxRetentionPolicy;

    private static int influxBatchSize;
    private static int influxBatchTimeMs;

    private static RateLimiter rateLimiter = null;
    private static InfluxConnectionPool influxConnectionPool = null;
    private static String PVFilterRegex = "";

    /**
     * Reads and validates property file.
     * Looks for: /opt/als/BPMLogScanner/BPMLogScanner.properties
     *
     * @throws IOException
     */
    private static void readProperties() throws IOException {
        if (appPath.toFile().exists() && appPath.toFile().isDirectory()) {
            Properties configuration = new Properties();
            configuration.load(Files.newInputStream(Paths.get(appPath.toString(), "BPMLogScanner.properties")));

            // Setup logger
            Path logPath = Paths.get(appPath.toString(), "log");
            if(!logPath.toFile().exists() || !logPath.toFile().isDirectory()) {
                logPath.toFile().mkdir();
            }
            FileHandler fh = new FileHandler(Paths.get(logPath.toString(), BPMLogToInflux.class.getName() + ".log").toString(), (int) 10e6, 5, true);
            fh.setFormatter(new SimpleFormatter());
            fh.setLevel(Level.INFO);
            logger.addHandler(fh);

            // Load the configs
            PVFilterRegex = configuration.getProperty("PVFilterRegex", "");
            influxURL = configuration.getProperty("influxURL");
            influxUser = configuration.getProperty("influxUser");
            influxPass = configuration.getProperty("influxPassword");
            influxDatabase = configuration.getProperty("influxDatabase");
            influxMeasurement = configuration.getProperty("influxMeasurement");
            influxRetentionPolicy = configuration.getProperty("influxRetentionPolicy");

            String batchSize = configuration.getProperty("influxBatchSize", "10000");
            String batchTime = configuration.getProperty("influxBatchTimeMs", "5");
            String logDir = configuration.getProperty("logDir");
            String threads = configuration.getProperty("threads");
            String sampleSpacing = configuration.getProperty("sampleSpacingMilliseconds");

            // Setup tuning params
            influxBatchSize = Integer.parseInt(batchSize);
            influxBatchSize = Math.min(Math.max(influxBatchSize, 10000), 10000000);
            influxBatchTimeMs = Integer.parseInt(batchTime);
            influxBatchTimeMs = Math.min(Math.max(influxBatchTimeMs, 1), 10000);

            // Setup rate limiter
            // Has a default value and a max value of 20.
            int transmitRateMBPerSec = Integer.parseInt(configuration.getProperty("transmitRateMBPerSec", "10"));
            transmitRateMBPerSec = Math.min(Math.max(transmitRateMBPerSec, 1), 20);
            double packetLimitRateBytes = transmitRateMBPerSec * 1e6;
            rateLimiter = RateLimiter.create(packetLimitRateBytes);

            // Verify configs.
            if (threads == null) {
                throw new IOException("Missing property (threads) in BPMLogScanner.properties");
            }
            try {
                NUM_THREADS = Integer.parseInt(threads);
            } catch (NumberFormatException e) {
                throw new IOException("Invalid format for property (threads)");
            }
            NUM_THREADS = Math.min(Math.max(NUM_THREADS, 1), Runtime.getRuntime().availableProcessors());

            if (logDir == null) {
                throw new IOException("Missing property (logdir) in BPMLogScanner.properties");
            }
            LOG_DIR = Paths.get(logDir);

            if (sampleSpacing == null) {
                throw new IOException("Missing property (sampleSpacingMilliseconds) in BPMLogScanner.properties");
            }
            try {
                SAMPLE_SPACING_MILLISECONDS = Integer.parseInt(sampleSpacing);
                SAMPLE_SPACING_MILLISECONDS = Math.max(0,SAMPLE_SPACING_MILLISECONDS);
            } catch (NumberFormatException e) {
                throw new IOException("Invalid format for property (sampleSpacingSeconds)");
            }
            if (!urlValidator.isValid(influxURL)) {
                throw new IOException("InfluxURL is not valid: " + influxURL);
            }
            if (influxURL == null) {
                throw new IOException("Missing property (influxURL) in BPMLogScanner.properties");
            }
            if (influxUser == null) {
                throw new IOException("Missing property (influxUser) in BPMLogScanner.properties");
            }
            if (influxPass == null) {
                throw new IOException("Missing property (influxPass) in BPMLogScanner.properties");
            }
            if (influxDatabase == null) {
                throw new IOException("Missing property (influxDatabase) in BPMLogScanner.properties");
            }
            if (influxMeasurement == null) {
                throw new IOException("Missing property (influxMeasurement) in BPMLogScanner.properties");
            }
        } else {
            throw new IOException("Application directory not found. Terminating.");
        }
    }

    public static void main(String[] args) throws Exception {
        readProperties();

        logger.info("Processing logs in: " + LOG_DIR.toFile().toURI().getPath() + " With " + NUM_THREADS + " thread(s)");

        // Setup Pooling resources
        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(NUM_THREADS);
        config.setMaxIdle(NUM_THREADS);
        influxConnectionPool = new InfluxConnectionPool(new InfluxFactory(
                influxURL, influxUser, influxPass, rateLimiter, true),
                config);

        // Verify connection to influx
        InfluxDB influxTest = null;
        try {
            influxTest = influxConnectionPool.borrowObject();
            Pong isAlive;
            try {
                isAlive = influxTest.ping();
            } catch (RetrofitError e) {
                throw new IOException("Unable to connect to InfluxDB", e);
            }
            logger.info("Connected to " + influxURL + " Version: " + isAlive.getVersion());
        } finally {
            influxConnectionPool.returnObject(influxTest);
        }

        // Send every directory to the processor.
        BPMLogCrawlerService crawlingService =
                new BPMLogCrawlerService(LOG_DIR,
                        influxConnectionPool, influxDatabase, influxMeasurement, influxRetentionPolicy, PVFilterRegex,
                        SAMPLE_SPACING_MILLISECONDS, NUM_THREADS, influxBatchSize, influxBatchTimeMs);
        crawlingService.startAsync().awaitTerminated(); // Wait for ever.

        System.exit(0);
    }
}
