package org.als.lbl.gov.bpmlogscanner;

import com.google.common.io.LittleEndianDataInputStream;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.HashMap;
import java.util.logging.Logger;

/**
 * Author: William Cork
 *
 * A class that is capable of reading records from the files used to log records at the BPMs.
 * These files have the form yyyy-MM-dd_HH:mm:ss.dat.bz2.
 * 
 * C-Structure data form:
 * struct bpmSlowAcquisition {
 *        epicsUInt32 magic;
 *        epicsUInt32 packetNumber;
 *        epicsUInt32 seconds;
 *        epicsUInt32 ticks;
 *        epicsUInt8  syncStatus;
 *        epicsUInt8  recorderStatus;
 *        epicsUInt8  clipStatus;
 *        epicsUInt8  cellCommStatus;
 *        epicsUInt8  autotrimStatus;
 *        epicsUInt8  pad1;
 *        epicsUInt8  pad2;
 *        epicsUInt8  pad3;
 *        epicsUInt16 adcPeak[BPM_PROTOCOL_ADC_COUNT];
 *        epicsUInt32 rfMag[BPM_PROTOCOL_ADC_COUNT];
 *        epicsUInt32 ptLoMag[BPM_PROTOCOL_ADC_COUNT];
 *        epicsUInt32 ptHiMag[BPM_PROTOCOL_ADC_COUNT];
 *        epicsUInt32 gainFactor[BPM_PROTOCOL_ADC_COUNT];
 *        epicsInt32  xPos;
 *        epicsInt32  yPos;
 *        epicsInt32  skew;
 *        epicsInt32  buttonSum;
 *        epicsInt32  xRMSwide;
 *        epicsInt32  yRMSwide;
 *        epicsInt32  xRMSnarrow;
 *        epicsInt32  yRMSnarrow;
 * };
 *      BPM_PROTOCOL_ADC_COUNT is 4.
 *
 */

public class BPMLogFile {
    private static final Logger LOGGER = Logger.getLogger(BPMLogFile.class.getName());

    static final long INT_MASK = 0xffffffffL;
    static final int SHORT_MASK = 0xffff;

    private static final int BPM_PROTOCOL_ADC_COUNT = 4;

    private final String bpmName;
    private Path inFile = null;
    private BufferedInputStream bis = null;
    private CompressorInputStream cis = null;

    private LittleEndianDataInputStream inStream = null;
    private long readPosition = 0;

    public BPMLogFile(String name, String path) throws ParseException, IOException, CompressorException {
        this.bpmName = name;
        this.inFile = Paths.get(path);

        this.bis = new BufferedInputStream(Files.newInputStream(this.inFile));
        this.cis = new CompressorStreamFactory().createCompressorInputStream(bis);
        this.inStream = new LittleEndianDataInputStream(this.cis);

        this.readPosition = 0;
    }

    /**
     * Closes the file.
     * @throws IOException if an I/O error occurs.
     */
    public void close() throws IOException {
        if(this.inStream != null) {
            this.inStream.close();
        }
        if (this.cis != null) {
            this.cis.close();
        }
        if (this.bis != null) {
            this.bis.close();
        }
    }

    /**
     * Efficiently skips a number of entries in the buffer.
     * @param numEntries the number of entries to skip.
     * @return true if read to desired skip distance.
     * @throws IOException if an I/O error occurs.
     */
    public boolean skipEntries(int numEntries) throws IOException {
        int desiredSkip = BPMLogEntry.SIZE_IN_BYTES * numEntries;
        int actualSkip = this.inStream.skipBytes(desiredSkip);
        return (desiredSkip == actualSkip);
    }

    /**
     * Will read an entry from the current position in the buffer.
     * @return The next entry.
     * @throws IOException when record is invalid or EOF.
     */
    public BPMLogEntry readNextEntry() throws IOException {
        BPMLogEntry bpmEntry = new BPMLogEntry(this.bpmName);

        bpmEntry.magic = this.inStream.readInt() & INT_MASK;

        if(bpmEntry.magic != 0xCAFE0004L) {
            throw new IOException(
                    String.format("Invalid magic number (0x%8X)", bpmEntry.magic));
        }

        bpmEntry.packetNumber = this.inStream.readInt() & INT_MASK;
        bpmEntry.seconds = this.inStream.readInt() & INT_MASK;
        bpmEntry.ticks = this.inStream.readInt() & INT_MASK;
        bpmEntry.syncStatus = this.inStream.readByte();
        bpmEntry.recorderStatus = this.inStream.readByte();
        bpmEntry.clipStatus = this.inStream.readByte();
        bpmEntry.cellCommStatus = this.inStream.readByte();
        bpmEntry.autotrimStatus = this.inStream.readByte();
        bpmEntry.pad1 = this.inStream.readByte();
        bpmEntry.pad2 = this.inStream.readByte();
        bpmEntry.pad3 = this.inStream.readByte();

        for(int x = 0; x < bpmEntry.adcPeak.length; x++) {
            bpmEntry.adcPeak[x] = this.inStream.readShort() & SHORT_MASK;
        }
        for(int x = 0; x < bpmEntry.rfMag.length; x++) {
            bpmEntry.rfMag[x] = this.inStream.readInt() & INT_MASK;
        }
        for(int x = 0; x < bpmEntry.ptLoMag.length; x++) {
            bpmEntry.ptLoMag[x] = this.inStream.readInt() & INT_MASK;
        }
        for(int x = 0; x < bpmEntry.ptHiMag.length; x++) {
            bpmEntry.ptHiMag[x] = this.inStream.readInt() & INT_MASK;
        }
        for(int x = 0; x < bpmEntry.gainFactor.length; x++) {
            bpmEntry.gainFactor[x] = this.inStream.readInt() & INT_MASK;
        }

        bpmEntry.xPos = this.inStream.readInt();
        bpmEntry.yPos = this.inStream.readInt();
        bpmEntry.skew = this.inStream.readInt();
        bpmEntry.buttonSum = this.inStream.readInt();
        bpmEntry.xRMSwide = this.inStream.readInt();
        bpmEntry.yRMSwide = this.inStream.readInt();
        bpmEntry.xRMSnarrow = this.inStream.readInt();
        bpmEntry.yRMSnarrow = this.inStream.readInt();

        this.readPosition += BPMLogEntry.SIZE_IN_BYTES;

        return bpmEntry;
    }

    /**
     * The entry of a single log.
     */
    public class BPMLogEntry {
        public static final int SIZE_IN_BYTES = 128;
        public static final double EVG_FREQUENCY = 499.64e6 / 4;

        //epicsUInt32 magic;
        public long magic;
        //epicsUInt32 packetNumber;
        public long packetNumber;
        //epicsUInt32 seconds;
        public long seconds;
        //epicsUInt32 ticks;
        public long ticks;
        //epicsUInt8  syncStatus;
        public byte syncStatus;
        //epicsUInt8  recorderStatus;
        public byte recorderStatus;
        //epicsUInt8  clipStatus;
        public byte clipStatus;
        //epicsUInt8  cellCommStatus;
        public byte cellCommStatus;
        //epicsUInt8  autotrimStatus;
        public byte autotrimStatus;
        //epicsUInt8  pad1;
        public byte pad1;
        //epicsUInt8  pad2;
        public byte pad2;
        //epicsUInt8  pad3;
        public byte pad3;
        //epicsUInt16 adcPeak[BPM_PROTOCOL_ADC_COUNT];
        public final int[] adcPeak = new int[BPM_PROTOCOL_ADC_COUNT];
        //epicsUInt32 rfMag[BPM_PROTOCOL_ADC_COUNT];
        public final long[] rfMag = new long[BPM_PROTOCOL_ADC_COUNT];
        //epicsUInt32 ptLoMag[BPM_PROTOCOL_ADC_COUNT];
        public final long[] ptLoMag = new long[BPM_PROTOCOL_ADC_COUNT];
        //epicsUInt32 ptHiMag[BPM_PROTOCOL_ADC_COUNT];
        public final long[] ptHiMag = new long[BPM_PROTOCOL_ADC_COUNT];
        //epicsUInt32 gainFactor[BPM_PROTOCOL_ADC_COUNT];
        public final long[] gainFactor = new long[BPM_PROTOCOL_ADC_COUNT];
        //epicsInt32  xPos;
        public int xPos;
        //epicsInt32  yPos;
        public int yPos;
        //epicsInt32  skew;
        public int skew;
        //epicsInt32  buttonSum;
        public int buttonSum;
        //epicsInt32  xRMSwide;
        public int xRMSwide;
        //epicsInt32  yRMSwide;
        public int yRMSwide;
        //epicsInt32  xRMSnarrow;
        public int xRMSnarrow;
        //epicsInt32  yRMSnarrow;
        public int yRMSnarrow;

        public final String BPM_NAME;

        BPMLogEntry(String name) {
            this.BPM_NAME = name;
        }

        /**
         * Packs all the values in the entry into a single structure.
         * @return HashMap of PV names -> value.
         */
        public HashMap<String, Number> getPVValues() {
            // TODO: Check for nulls.
            HashMap<String, Number> pvMap = new HashMap<>();

            // Unpack syncStatus
            pvMap.put(this.BPM_NAME + ":EVR:LO:sync", (this.syncStatus & 0x1b));
            pvMap.put(this.BPM_NAME + ":EVR:LossOfLock", ((this.syncStatus >> 1) & 0x1b));
            pvMap.put(this.BPM_NAME + ":EVR:AFEref:sync", ((this.syncStatus >> 2) & 0x1b));
            // Bit 3 is unused!
            pvMap.put(this.BPM_NAME + ":AFE:pllStatusLatch", ((this.syncStatus >> 4) & 0x1b));
            pvMap.put(this.BPM_NAME + ":EVR:SA:sync", ((this.syncStatus >> 5) & 0x1b));
            pvMap.put(this.BPM_NAME + ":EVR:FA:sync", ((this.syncStatus >> 6) & 0x1b));
            pvMap.put(this.BPM_NAME + ":AFE:pllStatus", ((this.syncStatus >> 7) & 0x1b));

            // Unpack recorderStatus
            pvMap.put(this.BPM_NAME + ":wfr:ADC:armed", (this.recorderStatus & 0x1b));
            pvMap.put(this.BPM_NAME + ":wfr:TBT:armed", ((this.recorderStatus >> 1) & 0x1b));
            pvMap.put(this.BPM_NAME + ":wfr:FA:armed", ((this.recorderStatus >> 2) & 0x1b));

            // Unpack clipStatus
            pvMap.put(this.BPM_NAME + ":ADC0:clip", (this.clipStatus & 0x1b));
            pvMap.put(this.BPM_NAME + ":ADC1:clip", ((this.clipStatus >> 1) & 0x1b));
            pvMap.put(this.BPM_NAME + ":ADC2:clip", ((this.clipStatus >> 2) & 0x1b));
            pvMap.put(this.BPM_NAME + ":ADC3:clip", ((this.clipStatus >> 3) & 0x1b));

            // Unpack cellCommStatus
            pvMap.put(this.BPM_NAME + ":Comm:CCW:channelUp", (this.cellCommStatus & 0x1b));
            pvMap.put(this.BPM_NAME + ":Comm:CCW:receiving", ((this.clipStatus >> 1) & 0x1b));
            pvMap.put(this.BPM_NAME + ":Comm:CW:channelUp", ((this.clipStatus >> 2) & 0x1b));
            pvMap.put(this.BPM_NAME + ":Comm:CW:receiving", ((this.clipStatus >> 3) & 0x1b));

            pvMap.put(this.BPM_NAME + ":autotrim:status", this.autotrimStatus);

            for(int x = 0; x < BPM_PROTOCOL_ADC_COUNT; x++) {
                pvMap.put(this.BPM_NAME + ":ADC"+x+":peak", this.adcPeak[x]);
                pvMap.put(this.BPM_NAME + ":ADC"+x+":ptHiMag", this.ptHiMag[x]);
                pvMap.put(this.BPM_NAME + ":ADC"+x+":ptLoMag", this.ptLoMag[x]);
                pvMap.put(this.BPM_NAME + ":ADC"+x+":rfMag", this.rfMag[x]);
                pvMap.put(this.BPM_NAME + ":ADC"+x+":gainFactor", this.gainFactor[x]);
            }
            pvMap.put(this.BPM_NAME + ":SA:X", this.xPos);
            pvMap.put(this.BPM_NAME + ":SA:Y", this.yPos);
            pvMap.put(this.BPM_NAME + ":SA:Q", this.skew);
            pvMap.put(this.BPM_NAME + ":SA:RMS:narrow:X", this.xRMSnarrow);
            pvMap.put(this.BPM_NAME + ":SA:RMS:narrow:Y", this.yRMSnarrow);
            pvMap.put(this.BPM_NAME + ":SA:RMS:wide:X", this.xRMSwide);
            pvMap.put(this.BPM_NAME + ":SA:RMS:wide:Y", this.yRMSwide);

            return pvMap;
        }

        /**
         * Calculate the full timestamp from the entry.
         * @return Timestamp with nanosecond precision.
         */
        public Timestamp getTimestamp() {
            // Precise time = seconds + (ticks / EVG_FREQUENCY)
            double tickMod = (this.ticks / EVG_FREQUENCY);
            double preciseTimeSeconds = (this.seconds + tickMod);
            double preciseTimeMillis = preciseTimeSeconds * 1000;
            int nanos = (int)((preciseTimeSeconds - (long)preciseTimeSeconds) * 1e9);
            Timestamp retTime = new Timestamp((long)preciseTimeMillis);
            retTime.setNanos(nanos);
            return retTime;
        }
    }
}
