package org.als.lbl.gov.bpmlogscanner;

import org.apache.commons.compress.compressors.CompressorException;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;

/**
 * This Processor iterates through a given path, determines which logFiles need to be processed, and pushes them to Influx.
 */
public class BPMLogDeviceDirectoryProcessor {
    private static final Logger logger = Logger.getLogger(BPMLogDeviceDirectoryProcessor.class.getName());

    private Preferences userPrefs = Preferences.userNodeForPackage(BPMLogDeviceDirectoryProcessor.class);

    private final DateFormat fileDateFmt = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private final int sampleSpacingMilliseconds;
    private final Path directory;

    private final String influxDatabase;
    private final String influxMeasurement;
    private final String influxRetentionPolicy;
    private final String PVFilterRegex;

    private InfluxDB influxConnection = null;

    public BPMLogDeviceDirectoryProcessor(Path directory, InfluxDB influxConnection, String influxDatabase,
                                          String influxMeasurement, String influxRetentionPolicy, String PVFilterRegex,
                                          int sampleSpacingMilliseconds) {
        try {
            String hostname = InetAddress.getLocalHost().getHostName();
            userPrefs = userPrefs.node(hostname);
        } catch (UnknownHostException e) {
            // ignore
        }

        this.directory = directory;
        this.influxConnection = influxConnection;
        this.influxDatabase = influxDatabase;
        this.influxMeasurement = influxMeasurement;
        this.influxRetentionPolicy = influxRetentionPolicy;
        this.PVFilterRegex = PVFilterRegex;
        this.sampleSpacingMilliseconds = sampleSpacingMilliseconds;

    }

    /**
     * Begins the processing of directory.
     * @throws IOException if an I/O error occurs
     */
    public void processDirectory() throws IOException {

        // Filters each file in log dir and gives only log files.
        // Executes
        List<File> logFiles = new ArrayList<>();
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(this.directory, "*.dat.bz2")) {
            ds.forEach(item -> {
                if (item.toFile().isFile()) {
                    logFiles.add(item.toFile());
                }
            });
        }

        // Important!!!!
        Collections.sort(logFiles);

        String iocName = this.directory.getFileName().toString();
        Preferences iocPrefs = userPrefs.node(iocName);
        Date fileDateTime = null;

        for(int x = 0; x < logFiles.size(); x++) {
            Date lastFileNameDate = new Date(iocPrefs.getLong("LAST_FILENAME_DATE", 0));
            Date lastFileModifiedTime = new Date(iocPrefs.getLong("LAST_FILE_MODIFIED_TIME", 0));
            int lastOffset = iocPrefs.getInt("LAST_OFFSET", 0);
            File file = logFiles.get(x);
            try {
                fileDateTime = new Date(this.fileDateFmt.parse(file.getName()).getTime());
            } catch (java.text.ParseException e) {
                logger.log(Level.SEVERE,
                        "Thread " + Thread.currentThread().getName() + ": Unable to parse logfile " +
                                file.getParentFile().getName() + "/" + file.getName(), e);
                continue;
            }

            // Have we processed newer?
            if (fileDateTime.before(lastFileNameDate)) {
                // We skip this file
                logger.config("Thread " + Thread.currentThread().getName() + ": Skipped (file too old): " +
                        file.getParentFile().getName() + "/" + file.getName());
                continue;
            }

            // Is this the current file?
            if (fileDateTime.getTime() == lastFileNameDate.getTime()) {
                // Has it been changed?
                if (!lastFileModifiedTime.before(new Date(file.lastModified()))) {
                    // No changes.
                    // Is this the last file?
                    if ((x+1) == logFiles.size()) {
                        logger.config("Thread " + Thread.currentThread().getName() + ": Skipped (No file changes): " +
                                file.getParentFile().getName() + "/" + file.getName());
                        continue;
                    } else {
                        // We actually finished this file.
                        logger.config("Thread " + Thread.currentThread().getName() + ": Skipped (Completed): " +
                                file.getParentFile().getName() + "/" + file.getName());
                        continue;
                    }
                }
            }

            // Is this a new file?
            if (fileDateTime.after(lastFileNameDate)) {
                lastOffset = 0;
                iocPrefs.putInt("LAST_OFFSET", lastOffset);
                logger.config("Thread " + Thread.currentThread().getName() + ": New File: " +
                        file.getParentFile().getName() + "/" + file.getName());
            }

            // Process the current file.
            try {
                lastOffset = processFile(iocName, file, lastOffset);
            } catch (BPMLogProcessException e) {
                //We want to skip bad files.
                logger.log(Level.WARNING, e.getMessage());
                // Is this the last file?
                if ((x + 1) == logFiles.size()) {
                    iocPrefs.putLong("LAST_FILENAME_DATE", fileDateTime.getTime());
                    iocPrefs.putLong("LAST_FILE_MODIFIED_TIME", file.lastModified());
                    iocPrefs.putInt("LAST_OFFSET", e.getFileOffset());
                    throw e;
                }
                else {
                    //Start at beginning of next file
                    lastOffset = 0;
                }
            }

            // Save last modifications
            iocPrefs.putLong("LAST_FILENAME_DATE", fileDateTime.getTime());
            iocPrefs.putLong("LAST_FILE_MODIFIED_TIME", file.lastModified());
            iocPrefs.putInt("LAST_OFFSET", lastOffset);
        }

    }

    /**
     * Processes the given file from the given offset.
     *
     * @param iocName String to define BPMLogFile PV names.
     * @param file The BMPLogFile
     * @param offset The starting offset of the given file
     * @return the new offset in this file.
     * @throws IOException
     */
    private int processFile(String iocName, File file, int offset) throws IOException {
        long process_start = System.currentTimeMillis();
        Timestamp lastSampleTime = new Timestamp(0);
        logger.info("Thread " + Thread.currentThread().getName() + ": Processing File: " +
                file.getParentFile().getName() + "/" + file.getName() + " ioc: " + iocName + " offset: " + offset);

        BPMLogFile logFile = null;
        int pointsWritten = 0;

        try {
            logFile = new BPMLogFile(iocName, file.getPath());
        } catch (java.text.ParseException | CompressorException e) {
            throw new IOException("Error with logfile " + file.getParentFile().getName() + "/" + file.getName(), e);
        }

        try {
            if (offset > 0) {
                // Go to the last entry we read.
                if (!logFile.skipEntries(offset - 1)) {
                    throw new IOException("Error with logfile " + file.getParentFile().getName() + "/" + file.getName() +
                            ". Could not skip to offset: " + offset);
                }
                BPMLogFile.BPMLogEntry lastEntry = logFile.readNextEntry();
                lastSampleTime = lastEntry.getTimestamp();  // For sample spacing
            }

            // Now we stream everything to Influx.
            BPMLogFile.BPMLogEntry entry;
            while ((entry = logFile.readNextEntry()) != null) {
                // Check sample spacing.
                while (entry.getTimestamp().before(new Timestamp(
                        lastSampleTime.getTime() + this.sampleSpacingMilliseconds))) {
                    entry = logFile.readNextEntry();
                    offset++;
                }

                // This is the entry to go to Influx.
                HashMap<String, Number> pvMap = entry.getPVValues();

                // We filter based on regex.
                Map<String, Number> filterMap = pvMap.entrySet().stream()
                        .filter(map -> !map.getKey().matches(PVFilterRegex))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

                Set<String> keys = filterMap.keySet();

                // Log entries have ns timestamps. We'll keep them.
                Timestamp entryTimestamp = entry.getTimestamp();
                long entryTimeMs = entryTimestamp.getTime();
                int entryTimeNs = entryTimestamp.getNanos();

                long MsShaped = entryTimeMs * (long)1e6;
                int NsShaped = entryTimeNs % (int)1e6;
                long totalNs = MsShaped + NsShaped;

                // Turn entry PVs into points.
                for (String key : keys) {
                    Point pnt = Point
                            .measurement(this.influxMeasurement)
                            .time(totalNs, TimeUnit.NANOSECONDS)
                            .tag("pvName", key)
                            .addField("value", filterMap.get(key))
                            .build();

                    // Send out.
                    this.influxConnection.write(this.influxDatabase, this.influxRetentionPolicy, pnt);
                    pointsWritten++;
                }
                lastSampleTime = entryTimestamp;
                offset++;
            }
        } catch (EOFException e) {
            // End of file. Ignore.
        } catch (IOException e) {
            throw new BPMLogProcessException("Error with file: " + file.getPath(), offset);
        } finally {
            logFile.close();
        }
        long process_end = System.currentTimeMillis();
        logger.info(String.format("Thread %s: Completed processing file: %s pointsWritten: %d in: %.2fs",
                Thread.currentThread().getName(), file.getParentFile().getName() + "/" + file.getName(),
                pointsWritten, (process_end - process_start)/1000.0));
        return offset;
    }

    public class BPMLogProcessException extends IOException
    {
        private int fileOffset;
        public BPMLogProcessException() {}
        public BPMLogProcessException(String message) {
            super(message);
        }
        public BPMLogProcessException(Exception e) {
            super(e);
        }
        public BPMLogProcessException(String message, int fileOffset) {
            super(message);
            this.fileOffset = fileOffset;
        }
        public int getFileOffset(){
            return this.fileOffset;
        }
    }
}
