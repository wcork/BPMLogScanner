package org.als.lbl.gov.bpmlogscanner;

import com.google.common.util.concurrent.AbstractScheduledService;
import org.als.lbl.gov.InfluxDB.InfluxConnectionPool;
import org.influxdb.InfluxDB;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Handles the directory crawl and parallelism to process BPMLogs in a given directory.
 */
public class BPMLogCrawlerService extends AbstractScheduledService {
    private static final Logger logger = Logger.getLogger(BPMLogCrawlerService.class.getName());
    private static ScheduledExecutorService EXECUTOR = null;

    private final int sampleSpacingMilliseconds;
    private final InfluxConnectionPool influxConnectionPool;
    private final String influxDatabase;
    private final String influxMeasurement;
    private final int influxBatchSize;
    private final int influxBatchTimeMs;
    private final String influxRetentionPolicy;
    private final String PVFilterRegex;

    private final Path logDir;

    private final Set<Path> taskPaths = new TreeSet<>();

    public BPMLogCrawlerService(Path logDir,
                                InfluxConnectionPool influxConnectionPool, String influxDatabase, String influxMeasurement,
                                String influxRetentionPolicy, String PVFilterRegex,
                                int sampleSpacingMilliseconds, int threadCount, int influxBatchSize, int influxBatchTimeMs) {
        this.logDir = logDir;
        this.influxConnectionPool = influxConnectionPool;
        this.influxDatabase = influxDatabase;
        this.influxMeasurement = influxMeasurement;
        this.sampleSpacingMilliseconds = sampleSpacingMilliseconds;
        this.influxBatchSize = influxBatchSize;
        this.influxBatchTimeMs = influxBatchTimeMs;
        this.influxRetentionPolicy = influxRetentionPolicy;
        this.PVFilterRegex = PVFilterRegex;

        EXECUTOR = Executors.newScheduledThreadPool(threadCount);
    }

    @Override
    protected void runOneIteration() throws Exception {
        // Update the device dir (We trust the higher level this path exists)
        try (Stream<Path> devicePathStream = Files.list(logDir).filter(path -> path.toFile().isDirectory())) {
            Set<Path> devicePaths = devicePathStream.collect(Collectors.toSet());

            Object[] taskList = taskPaths.toArray();
            for (int x = 0; x < taskPaths.size(); x++) {
                Path cTaskPath = (Path) taskList[x];
                // Remove taskPaths that are not in devicePaths
                if (!devicePaths.contains(cTaskPath)) {
                    taskPaths.remove(cTaskPath);
                }
                // Make devicePaths only contain paths we aren't scanning yet
                devicePaths.remove(cTaskPath);
            }

            // Update task path set
            taskPaths.addAll(devicePaths);

            // Schedule only the new paths found.
            devicePaths.forEach(path -> EXECUTOR.scheduleAtFixedRate(new CrawlerSubTask(path), 0, 1000, TimeUnit.MILLISECONDS));
        }
    }

    @Override
    protected Scheduler scheduler() {
        return Scheduler.newFixedRateSchedule(0, 1, TimeUnit.SECONDS);
    }

    /**
     * Task that actually crawls the device directory.
     */
    private class CrawlerSubTask implements Runnable, Comparable<CrawlerSubTask> {

        private final Path path;

        CrawlerSubTask(Path path){
            this.path = path;
        }

        @Override
        public void run() {
            InfluxDB influxConnection = null;
            try {
                influxConnection = influxConnectionPool.borrowObject();

                if (influxConnection.isBatchEnabled()) {
                    influxConnection.disableBatch();
                }
                influxConnection.enableBatch(influxBatchSize, influxBatchTimeMs, TimeUnit.MILLISECONDS);

                BPMLogDeviceDirectoryProcessor processor =
                        new BPMLogDeviceDirectoryProcessor(this.path,
                                influxConnection, influxDatabase, influxMeasurement, influxRetentionPolicy,
                                PVFilterRegex, sampleSpacingMilliseconds);
                processor.processDirectory();
            } catch (BPMLogDeviceDirectoryProcessor.BPMLogProcessException e) {
                logger.log(Level.INFO, "Caught up to end of dir.");
            } catch (Exception e) {
                logger.log(Level.SEVERE, "Error processing directory: " + path, e);
            } finally {
                if (influxConnection != null && influxConnection.isBatchEnabled()) {
                    influxConnection.disableBatch();
                }
                influxConnectionPool.returnObject(influxConnection);
            }
        }

        @Override
        public int compareTo(CrawlerSubTask o) {
            return this.path.compareTo(o.path);
        }

        public Path getPath() {
            return this.path;
        }
    }

}
