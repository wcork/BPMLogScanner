package org.als.lbl.gov.bpmlogscanner;

import org.apache.commons.compress.compressors.CompressorException;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Point;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * TODO: Remove this and make unit tests.
 * Scratch test for BPMLogFile
 */
public class TestBPMLogFile {
    private static final String fileName = "";


    public static void main(String[] args) throws java.text.ParseException, CompressorException, IOException {
        //Use Apache Commons-CLI to parse args.


        Path file = Paths.get(fileName);
        String bpmName = file.getParent().getFileName().toString();

        BPMLogFile logFile = new BPMLogFile(bpmName, file.toAbsolutePath().toString());

        // For time test
        InfluxDB influx = InfluxDBFactory.connect("http://localhost:8086","user","pass");
        BPMLogFile.BPMLogEntry entry = logFile.readNextEntry();
        HashMap<String, Number> values = entry.getPVValues();
        Set<String> setKeys = values.keySet();
        System.out.println("====================== Entry1 ======================");
        for(String value : setKeys) {
            Point pnt = Point
                    .measurement("BPM")
                    .tag("pvName", value)
                    .time(entry.getTimestamp().getTime(), TimeUnit.MILLISECONDS)

                    .addField("value", values.get(value))
                    .build();
            System.out.println(pnt.lineProtocol());
        }

        System.out.println();
        System.out.println("Skipping Entries");
        logFile.skipEntries(Integer.MAX_VALUE/200);
        System.out.println();

        BPMLogFile.BPMLogEntry entrySkip = logFile.readNextEntry();
        HashMap<String, Number> valuesSkip = entrySkip.getPVValues();
        Set<String> setKeysSkip = values.keySet();
        System.out.println("====================== Entry2 ======================");
        for(String value : setKeysSkip) {
            Point pnt = Point
                    .measurement("BPM")
                    .tag("pvName", value)
                    .time(entrySkip.getTimestamp().getTime(), TimeUnit.MILLISECONDS)

                    .addField("value", valuesSkip.get(value))
                    .build();
            System.out.println(pnt.lineProtocol());
        }

        System.exit(0);
    }
}
